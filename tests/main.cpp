#include <rili/DSDL.hpp>
#include <rili/Test.hpp>
#include <string>

TEST(DSDL, serializeAndDeserializeTwice) {
    std::string dsdl =
        "  const MAGIC_NUMBER = 5;\n"
        "  enum SomeEnum\n"
        "  {\n"
        "      const a = 1;\n"
        "      # some comment\n"
        "      const b = (MAGIC_NUMBER - 2);\n"
        "  };\n"
        "  \n"
        "  struct A\n"
        "  {\n"
        "    struct B\n"
        "    {\n"
        "       var u32 v;\n"
        "    };\n"
        "  };\n"
        "  \n"
        "  # some comment\n"
        "  struct SomeStruct\n"
        "  {\n"
        "      const SOME_CONST = (5 + MAGIC_NUMBER);\n"
        "      var i32 a;\n"
        "      array i8 b[1234];\n"
        "      array u16 c[MAGIC_NUMBER];\n"
        "      struct SomeOtherStruct\n"
        "      {\n"
        "          var u16 a;\n"
        "          array u16 b[SOME_CONST];\n"
        "      };\n"
        "      var SomeOtherStruct d;\n"
        "      var SomeOtherStruct e;\n"
        "      var .A.B f;\n"
        "  };\n"
        "  \n"
        "  option MyOption{\n"
        "      const SOME_CONST = (5 + MAGIC_NUMBER);\n"
        "      var i32 a;\n"
        "      struct SomeOtherStruct\n"
        "      {\n"
        "          var u16 a;\n"
        "          array u16 b[SOME_CONST];\n"
        "      };\n"
        "      var SomeOtherStruct b;\n"
        "      var .A.B c;\n"
        "  };\n";

    rili::DSDL document;
    document.parse(dsdl);
    auto x = document.stringify();
    document.parse(x);
    auto y = document.stringify();
    EXPECT_EQ(x, y);
}

int main(int, char**) { return rili::test::runner::run() ? 0 : 1; }
