#include <cstddef>
#include <fstream>
#include <iostream>
#include <list>
#include <set>
#include <string>

namespace {
std::list<std::string> split(std::string const& path) {
    std::list<std::string> splited;
    std::string current;

    for (auto c : path) {
        if (c == '/') {
            if (!current.empty()) {
                splited.push_back(current);
            }
            current.clear();
        } else {
            current += c;
        }
    }
    if (!current.empty()) {
        splited.push_back(current);
    }

    while (!splited.empty() && splited.back().empty()) {
        splited.pop_back();
    }
    return splited;
}

std::string merge(std::list<std::string> path) {
    std::string result;

    while (path.size() > 1) {
        result += path.front() + "/";
        path.pop_front();
    }
    result += path.front();
#ifndef _WINDOWS
    if (result[0] != '.' && result[0] != '/') {
        return "/" + result;
    }
#endif
    return result;
}

std::string join(const std::string& rootPath, const std::string& subpath) {
    auto rootElements = split(rootPath);
    auto subpathElements = split(subpath);

    if (!rootElements.empty() && rootElements.back().empty()) {
        rootElements.pop_back();
    }

    if (!subpathElements.empty() && subpathElements.front().empty()) {
        subpathElements.pop_front();
    }
    rootElements.insert(rootElements.end(), subpathElements.begin(), subpathElements.end());

    return merge(rootElements);
}

std::string cannonical(std::string const& path) {
    const auto splitted = split(path);
    std::list<std::string> result;

    for (auto const& e : splitted) {
        if (e == ".." && !result.empty() && result.back() != "..") {
            result.pop_back();
        } else {
            result.push_back(e);
        }
    }
    return merge(result);
}

std::string dirname(std::string const& path) {
    auto splited = split(path);
    if (splited.size() > 1) {
        splited.pop_back();
    }
    return merge(splited);
}

std::string parseIncludeLine(std::string const& includeLine) {
    char const* begin = includeLine.c_str() + 2;
    char const* max = includeLine.c_str() + includeLine.size();
    while (begin < max && (*begin == ' ' || *begin == '\t' || *begin == '\f' || *begin == '\v')) {
        begin++;
    }

    char const* end = max - 1;
    while (end > begin &&
           (*end == ' ' || *end == '\t' || *end == '\f' || *end == '\v' || *end == '\r' || *end == '\n')) {
        end--;
    }
    end++;
    if (end > begin) {
        return std::string(begin, end);
    } else {
        return {};
    }
}

std::string getIncludePath(std::string const& currentFilePath, std::string const& includeLine) {
    return cannonical(join(dirname(currentFilePath), parseIncludeLine(includeLine)));
}

bool run(bool withDebug, std::size_t depth, std::string const& parentPath, std::size_t parentLine,
         std::string const& currentPath, std::set<std::string>& alreadyIncluded) {
    alreadyIncluded.insert(currentPath);
    std::ifstream currentFile(currentPath, std::ios::in | std::ios::binary);
    if (currentFile.is_open() && currentFile.good()) {
        std::string line;
        std::size_t lineNumber = 0;
        while (std::getline(currentFile, line)) {
            if (line.size() > 2 && line[0] == '#' && line[1] == '@') {
                auto toInclude = getIncludePath(currentPath, line);
                if (alreadyIncluded.find(toInclude) == alreadyIncluded.end()) {
                    if (withDebug) {
                        std::string padding(depth * 2, ' ');
                        std::cout << padding << "#@ begin " << currentPath << ":" << lineNumber << " -> " << toInclude
                                  << std::endl;
                        if (!run(true, depth + 1, currentPath, lineNumber, toInclude, alreadyIncluded)) {
                            return false;
                        }
                        std::cout << padding << "#@ end " << currentPath << ":" << lineNumber << " -> " << toInclude
                                  << std::endl;
                    } else {
                        if (!run(false, depth + 1, currentPath, lineNumber, toInclude, alreadyIncluded)) {
                            return false;
                        }
                    }
                }
            } else {
                std::cout << line << std::endl;
            }
            lineNumber++;
        }
        return true;
    } else {
        std::cerr << "rili-dsdl-preprocessor: could not find file(" << currentPath << ") required at " << parentPath
                  << ":" << parentLine << std::endl;
        return false;
    }
}

int run(bool withDebug, std::string const& path) {
    std::set<std::string> alreadyIncluded;

    if (run(withDebug, 0, "rili-dsdl-preprocessor@commandline", 0, path, alreadyIncluded)) {
        return 0;
    } else {
        return -1;
    }
}

}  // namespace

int main(int argc, char** argv) {
    if (argc != 3 && argc != 2) {
        std::cerr << "usage: \n  ./rili-dsdl-preprocessor {DEBUG} file-path.dsdl" << std::endl;
        return -1;
    } else {
        if (argc == 3) {
            std::string type(argv[1]);
            if (type == "DEBUG") {
                return run(true, argv[2]);
            } else {
                return -1;
            }
        } else {
            return run(false, argv[1]);
        }
    }
}
