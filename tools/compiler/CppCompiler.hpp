#include <list>
#include <map>
#include <rili/DSDL.hpp>
#include <string>
#include <utility>

namespace rili {
namespace dsdl {
namespace compiler {

class CppCompiler : public rili::DSDL::Traverser {
 public:
    void emitRootBegin() override;
    void emitRootEnd() override;
    void emitEnumeratorBegin(std::string const& name) override;
    void emitEnumeratorEnd(std::string const& name) override;
    void emitStructureBegin(std::string const& name) override;
    void emitStructureEnd(std::string const& name) override;
    void emitComment(std::string const& value) override;
    void emitVariable(std::string const& name, std::string const& type) override;
    void emitConstant(std::string const& name, std::int64_t expr) override;
    void emitArray(std::string const& name, std::string const& type, std::uint64_t sizeExpr) override;
    void emitOptionBegin(std::string const& name) override;
    void emitOptionEnd(std::string const& name) override;

    void emitOptionAlternatives(std::list<std::pair<std::string, std::string>> const& alternatives) override;
    void emitEnumeratorEntry(std::string const& name, std::int64_t expr) override;

 private:
    bool isBuildinType(std::string const& type) const;
    std::string fixTypeIfNeeded(std::string const& type) const;
    std::string getQualifiedName(std::list<std::string> const& names) const;
    std::string getPadding() const { return std::string(m_structStack.size() * 4, ' '); }

 public:
    CppCompiler(CppCompiler const&) = delete;
    CppCompiler& operator=(CppCompiler const&) = delete;
    CppCompiler();
    explicit CppCompiler(std::list<std::string> const& customNamespace);
    ~CppCompiler() override = default;

    inline std::string getStructDefinitions() const { return m_structDefinitionsBuffer; }
    inline std::string getFunctionDefinitions() const { return m_functionDefinitionsBuffer; }

 private:
    std::list<std::pair<std::string, std::string>> m_structStack;

    std::string m_structDefinitionsBuffer;
    std::string m_functionDefinitionsBuffer;
    std::list<std::string> m_namespace;
};
}  // namespace compiler
}  // namespace dsdl
}  // namespace rili
