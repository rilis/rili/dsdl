#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <memory>
#include <rili/MakeUnique.hpp>
#include <sstream>
#include <string>
#include <tools/compiler/CppCompiler.hpp>
#include <vector>

namespace {

bool getOptions(std::map<std::string, std::string>& options, int argc, char** argv) {
    for (auto i = 3; i < argc; i++) {
        std::stringstream ss(argv[i]);
        std::vector<std::string> result;

        while (ss.good()) {
            std::string substr;
            getline(ss, substr, '=');
            if (!substr.empty()) {
                result.push_back(substr);
            }
        }

        if (result.size() != 2) {
            return false;
        } else {
            options[result[0]] = result[1];
        }
    }

    return true;
}

bool writeToFile(std::string const& path, std::string const& data) {
    std::ofstream os(path);
    if (os) {
        os << data << std::flush;
        return true;
    } else {
        return false;
    }
}

std::string readWholeStdin() {
    std::cin >> std::noskipws;
    std::istream_iterator<char> it(std::cin);
    std::istream_iterator<char> end;
    return std::string(it, end);
}

std::list<std::string> splitByDot(std::string const& s) {
    std::list<std::string> result;
    std::string current;
    for (auto const& c : s) {
        if (c != '.') {
            current.push_back(c);
        } else if (!current.empty()) {
            result.push_back(current);
            current.clear();
        }
    }
    if (!current.empty()) {
        result.push_back(current);
        current.clear();
    }
    return result;
}

bool generateCpp(rili::DSDL::RootType& document, std::string const& destinationDirectoryPath,
                 std::map<std::string, std::string> const& options) {
    try {
        std::unique_ptr<rili::dsdl::compiler::CppCompiler> generator;

        auto iter = options.find("namespace");
        if (iter != options.end()) {
            generator = rili::make_unique<rili::dsdl::compiler::CppCompiler>(splitByDot(iter->second));
        } else {
            generator = rili::make_unique<rili::dsdl::compiler::CppCompiler>();
        }

        generator->run(document);

        auto const structPath = destinationDirectoryPath + "/DSDL.structures.hpp";
        auto const funcPath = destinationDirectoryPath + "/DSDL.functions.hpp";
        if (writeToFile(structPath, generator->getStructDefinitions()) &&
            writeToFile(funcPath, generator->getFunctionDefinitions())) {
            return true;
        } else {
            std::cerr << "rili-dsdl-compiler: CPP code generator cannot write results (" << structPath << ", "
                      << funcPath << ")!" << std::endl;
            return false;
        }
    } catch (rili::DSDL::SyntaxError const& error) {
        std::cerr << "rili-dsdl-compiler: CPP code generation error!" << error.what() << std::endl;
        return false;
    }
}

std::size_t getErrorLine(std::string const& source, std::size_t position) {
    std::size_t line = 0;
    for (std::size_t i = 0; i < position; i++) {
        if (source[i] == '\n') {
            line++;
        }
    }
    return line + 1;
}
}  // namespace

int main(int argc, char** argv) {
    rili::DSDL root;
    std::map<std::string, std::string> options;

    if (argc < 3 || !getOptions(options, argc, argv)) {
        std::cerr << "rili-dsdl-compiler usage: \n  ./rili-dsdl-compiler CPP DESTINATION_DIRECTORY "
                     "[OPTION=VALUE ...] < definitions.dsdl"
                  << std::endl;
        return -1;
    } else {
        const std::string language(argv[1]);
        const std::string sourceData(readWholeStdin());
        const std::string destinationDir(argv[2]);

        try {
            root.parse(sourceData);

            if (language == "CPP") {
                return generateCpp(root.root(), destinationDir, options) ? 0 : -4;
            } else {
                std::cerr << "rili-dsdl-compiler: Language: " << language << " is unknown" << std::endl;
                return -3;
            }
        } catch (rili::DSDL::SyntaxError const& error) {
            std::cerr << "rili-dsdl-compiler: Parsing error at line: " << getErrorLine(sourceData, error.position())
                      << "(" << error.position() << ")" << std::endl;
            return -2;
        }
    }
}
