#include <list>
#include <string>
#include <tools/compiler/CppCompiler.hpp>
#include <utility>

namespace rili {
namespace dsdl {
namespace compiler {

void CppCompiler::emitRootBegin() {
    m_structDefinitionsBuffer +=
        "#pragma once\n"
        "#include <rili/dsdl/Primitives.hpp>\n\n";
    for (auto const& n : m_namespace) {
        m_structDefinitionsBuffer += "namespace " + n + " {\n";
    }

    m_structDefinitionsBuffer += "using namespace ::rili::dsdl;\n\n";
    m_functionDefinitionsBuffer +=
        "#pragma once\n"
        "#include <rili/dsdl/Primitives.hpp>\n"
        "#include \"DSDL.structures.hpp\"\n\n";
    for (auto const& n : m_namespace) {
        m_functionDefinitionsBuffer += "namespace " + n + " {\n";
    }

    m_functionDefinitionsBuffer += "using namespace ::rili::dsdl;\n\n";
}

void CppCompiler::emitRootEnd() {
    for (auto it = m_namespace.rbegin(); it != m_namespace.rend(); it++) {
        m_structDefinitionsBuffer += "} // namespace " + *it + "\n";
    }
    for (auto it = m_namespace.rbegin(); it != m_namespace.rend(); it++) {
        m_functionDefinitionsBuffer += "} // namespace " + *it + "\n";
    }
}

void CppCompiler::emitEnumeratorBegin(const std::string& name) {
    m_structDefinitionsBuffer += getPadding() + "enum class " + name + " : " + "u32 {\n";

    const auto qname = getQualifiedName(parentNames());

    {
        m_functionDefinitionsBuffer += "inline bool serialize(" + qname + " const& e, Buffer & buffer) {\n" +
                                       "    return serialize(u32(e), buffer);\n"
                                       "}\n\n";
        m_functionDefinitionsBuffer += "inline bool deserialize(" + qname + "& e, Buffer & buffer) {\n" +
                                       "    u32 tmp;\n"
                                       "    if(deserialize(tmp, buffer)) {\n" +
                                       "        e = static_cast<" + qname + ">(tmp);\n" +
                                       "        return true;\n"
                                       "    }\n"
                                       "    else {\n"
                                       "        return false;\n"
                                       "    }\n"
                                       "}\n\n";
    }
}

void CppCompiler::emitEnumeratorEnd(const std::string& name) {
    m_structDefinitionsBuffer += getPadding() + "    __last\n";
    m_structDefinitionsBuffer += getPadding() + "}; // enum " + name + "\n\n";
}

void CppCompiler::emitStructureBegin(const std::string& name) {
    m_structDefinitionsBuffer += getPadding() + "struct " + name + " {\n";
    const auto qname = getQualifiedName(parentNames());
    {
        m_structStack.push_back({{}, {}});
        auto& currentSerializeBuffer = m_structStack.back().first;
        auto& currentDeserializeBuffer = m_structStack.back().second;
        currentSerializeBuffer += "inline bool serialize(" + qname + " const& s, Buffer & buffer) {\n";
        currentDeserializeBuffer += "inline bool deserialize(" + qname + " & s, Buffer & buffer) {\n";
    }
}

void CppCompiler::emitStructureEnd(const std::string& name) {
    {
        auto& currentSerializeBuffer = m_structStack.back().first;
        auto& currentDeserializeBuffer = m_structStack.back().second;

        m_functionDefinitionsBuffer += currentSerializeBuffer +
                                       "\n    (void)(s);"
                                       "\n    (void)(buffer);"
                                       "\n    return true;\n}\n\n";
        m_functionDefinitionsBuffer += currentDeserializeBuffer +
                                       "\n    (void)(s);"
                                       "\n    (void)(buffer);"
                                       "\n    return true;\n}\n\n";
        m_structStack.pop_back();
    }
    m_structDefinitionsBuffer += getPadding() + "}; // struct " + name + "\n\n";
}

void CppCompiler::emitComment(const std::string& value) {
    m_structDefinitionsBuffer += getPadding() + "//" + value + "\n";
}

void CppCompiler::emitVariable(const std::string& name, const std::string& type) {
    m_structDefinitionsBuffer += getPadding() + fixTypeIfNeeded(type) + " " + name + ";\n";

    {
        auto& csb = m_structStack.back().first;
        csb += "    if(!serialize(s." + name + ", buffer)) { return false; }\n";
    }
    {
        auto& cdb = m_structStack.back().second;
        cdb += "    if(!deserialize(s." + name + ", buffer)) { return false; }\n";
    }
}

void CppCompiler::emitConstant(const std::string& name, std::int64_t val) {
    m_structDefinitionsBuffer += getPadding() + "auto static constexpr " + name + " = " + std::to_string(val) + ";\n";
}

void CppCompiler::emitArray(const std::string& name, const std::string& type, std::uint64_t sizeExpr) {
    const auto padding = getPadding();
    m_structDefinitionsBuffer += padding + "u32 _" + name + "Size;\n";
    m_structDefinitionsBuffer +=
        padding + "auto static constexpr _" + name + "MaxSize = " + std::to_string(sizeExpr) + ";\n";
    m_structDefinitionsBuffer += padding + fixTypeIfNeeded(type) + " " + name + "[_" + name + "MaxSize];\n";
    {
        {
            auto& csb = m_structStack.back().first;
            csb += "    if(s._" + name + "Size > s._" + name + "MaxSize) { return false; }\n";
            csb += "    if(!serialize(s._" + name + "Size, buffer)) { return false; }\n";

            csb += "    for(u32 i = 0; i < s._" + name + "Size; i++) {\n";
            csb += "        if(!serialize(s." + name + "[i], buffer)) { return false; }\n";
            csb += "    }\n";
        }
        {
            auto& cdb = m_structStack.back().second;
            cdb += "    if(!deserialize(s._" + name + "Size, buffer)) { return false; }\n";
            cdb += "    if(s._" + name + "Size > s._" + name + "MaxSize) { return false; }\n";

            cdb += "    for(u32 i = 0; i < s._" + name + "Size; i++) {\n";
            cdb += "        if(!deserialize(s." + name + "[i], buffer)) { return false; }\n";
            cdb += "    }\n";
        }
    }
}

void CppCompiler::emitOptionBegin(const std::string& name) {
    m_structDefinitionsBuffer += getPadding() + "struct " + name + " {\n";

    const auto qname = getQualifiedName(parentNames());
    {
        m_structStack.push_back({{}, {}});
        auto& currentSerializeBuffer = m_structStack.back().first;
        auto& currentDeserializeBuffer = m_structStack.back().second;
        currentSerializeBuffer += "inline bool serialize(" + qname + " const& o, Buffer & buffer) {\n";
        currentDeserializeBuffer += "inline bool deserialize(" + qname + " & o, Buffer & buffer) {\n";
    }
}
void CppCompiler::emitOptionEnd(const std::string& name) {
    {
        auto& currentSerializeBuffer = m_structStack.back().first;
        auto& currentDeserializeBuffer = m_structStack.back().second;

        m_functionDefinitionsBuffer += currentSerializeBuffer + "}\n\n";
        m_functionDefinitionsBuffer += currentDeserializeBuffer + "}\n\n";
        m_structStack.pop_back();
    }
    m_structDefinitionsBuffer += getPadding() + "}; // option " + name + "\n\n";
}
void CppCompiler::emitOptionAlternatives(const std::list<std::pair<std::string, std::string>>& alternatives) {
    {
        const auto padding = getPadding();
        {
            m_structDefinitionsBuffer += padding + "enum class _Selector : u32 {\n";
            std::size_t i = 0;
            for (auto const& a : alternatives) {
                m_structDefinitionsBuffer += padding + "    " + a.second + " = " + std::to_string(i) + ",\n";
                i++;
            }
            m_structDefinitionsBuffer += padding + "    __last = " + std::to_string(i) + "\n";
            m_structDefinitionsBuffer += padding + "};\n";
        }

        {
            m_structDefinitionsBuffer += padding + "union _Alternatives {\n";
            for (auto const& a : alternatives) {
                m_structDefinitionsBuffer += padding + "    " + fixTypeIfNeeded(a.first) + " " + a.second + ";\n";
            }
            m_structDefinitionsBuffer += padding + "};\n";
        }
        m_structDefinitionsBuffer += padding + "_Selector _selected;\n";
        m_structDefinitionsBuffer += padding + "_Alternatives _alternative;\n";
    }
    {
        if (alternatives.empty()) {
            auto& currentSerializeBuffer = m_structStack.back().first;
            auto& currentDeserializeBuffer = m_structStack.back().second;
            currentSerializeBuffer += "    return true;\n";
            currentDeserializeBuffer += "    return true;\n";
        } else {
            {
                auto& currentSerializeBuffer = m_structStack.back().first;
                currentSerializeBuffer +=
                    "    if(!serialize(u32(o._selected), buffer)){ return false; }\n"
                    "    switch(o._selected) {\n";
                for (auto const& a : alternatives) {
                    currentSerializeBuffer +=
                        "        case " + parentNames().back() + "::_Selector::" + a.second + ": { ";
                    const auto isBuildIn = isBuildinType(a.first);
                    {
                        if (isBuildIn) {
                            currentSerializeBuffer += "return serialize(o._alternative." + a.second + ", buffer);} \n";
                        } else {
                            currentSerializeBuffer += "return serialize(o._alternative." + a.second + ", buffer); }\n";
                        }
                    }
                }
                currentSerializeBuffer += "        default: { return false; }\n";
                currentSerializeBuffer += "    }\n";
            }
            {
                auto& currentDeserializeBuffer = m_structStack.back().second;
                currentDeserializeBuffer +=
                    "    u32 tmp;\n"
                    "    if(deserialize(tmp, buffer)) {\n"
                    "        o._selected = static_cast<" +
                    parentNames().back() + "::_Selector>(tmp);\n" +
                    "    }\n"
                    "    else {\n"
                    "        return false;\n"
                    "    }\n"
                    "    switch(o._selected) {\n";
                for (auto const& a : alternatives) {
                    currentDeserializeBuffer +=
                        "        case " + parentNames().back() + "::_Selector::" + a.second + ": { ";

                    currentDeserializeBuffer += "return deserialize(o._alternative." + a.second + ", buffer); }\n";
                }
                currentDeserializeBuffer += "        default: { return false; }\n";
                currentDeserializeBuffer += "    }";
            }
        }
    }
}
void CppCompiler::emitEnumeratorEntry(const std::string& name, std::int64_t expr) {
    m_structDefinitionsBuffer += getPadding() + "    " + name + " = " + std::to_string(expr) + ",\n";
}

bool CppCompiler::isBuildinType(const std::string& type) const {
    return type == "u8" || type == "u16" || type == "u32" || type == "u64" || type == "i8" || type == "i16" ||
           type == "i32" || type == "i64";
}

std::string CppCompiler::fixTypeIfNeeded(const std::string& type) const {
    std::string result;
    if (type[0] == '.') {
        for (auto const& n : m_namespace) {
            result += "::" + n;
        }
    }
    for (auto c : type) {
        if (c == '.') {
            result += "::";
        } else {
            result += c;
        }
    }
    return result;
}

std::string CppCompiler::getQualifiedName(const std::list<std::string>& names) const {
    std::string result;
    if (!names.empty()) {
        for (auto const& n : names) {
            result += n + "::";
        }
        result.resize(result.size() - 2);
    }
    return result;
}

CppCompiler::CppCompiler() : rili::DSDL::Traverser(), m_namespace({}) {}
CppCompiler::CppCompiler(const std::list<std::string>& customNamespace) : CppCompiler() {
    m_namespace.insert(m_namespace.end(), customNamespace.begin(), customNamespace.end());
}

}  // namespace compiler
}  // namespace dsdl
}  // namespace rili
