#include <fstream>
#include <iostream>
#include <iterator>
#include <rili/DSDL.hpp>
#include <streambuf>
#include <string>

namespace {
bool readWholeFile(std::string const& path, std::string& data) {
    std::ifstream t(path);
    if (t.is_open() && t.good()) {
        data = std::string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
        return true;
    } else {
        return false;
    }
}

bool writeWholeFile(std::string const& path, std::string const& data) {
    std::ofstream t(path);
    if (t.is_open() && t.good()) {
        t << data << std::flush;
        return true;
    } else {
        return false;
    }
}

std::size_t getErrorLine(std::string const& source, std::size_t position) {
    std::size_t line = 0;
    for (std::size_t i = 0; i < position; i++) {
        if (source[i] == '\n') {
            line++;
        }
    }
    return line + 1;
}
}  // namespace

int main(int argc, char** argv) {
    if (argc != 3) {
        std::cerr << "usage:/n  ./rili-dsdl-format <IN_FILE> <OUT_FILE>" << std::endl;
    } else {
        rili::DSDL document;
        std::string in;
        if (readWholeFile(argv[1], in)) {
            try {
                document.parse(in);
                if (writeWholeFile(argv[2], document.stringify())) {
                    return 0;
                } else {
                    std::cerr << "rili-dsdl-format: cannot write to file: " << argv[2] << std::endl;
                }
            } catch (rili::DSDL::SyntaxError const& error) {
                std::cerr << "rili-dsdl-format: Parsing error at line: " << getErrorLine(in, error.position()) << "("
                          << error.position() << ")" << std::endl;
                return -1;
            }
        } else {
            std::cerr << "rili-dsdl-format: cannot read from file: " << argv[1] << std::endl;
            return -2;
        }
    }
}
