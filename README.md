# Rili DSDL

This is module of [rili](https://gitlab.com/rilis/rili) which contain implementation of DSDL language parser.

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/dsdl)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/dsdl/badges/master/build.svg)](https://gitlab.com/rilis/rili/dsdl/commits/master)
[![coverage report](https://gitlab.com/rilis/rili/dsdl/badges/master/coverage.svg)](https://gitlab.com/rilis/rili/dsdl/commits/master)

# DSDL syntax cheat sheet

## Comment

```
# this is my comment
```

Comment parser expr: `#[^\n]*\n`

**note:** comments cannot exist in middle of other expressions

This is not valid DSDL code as comment here is in middle of enum begin expr block:

```
enum MyEnum # this is my enum
{
};
```

## Constant

```
const MY_CONST = MY_OTHER_CONST_VISIBLE_IN_CURRENT_SCOPE + 5;
```

Constant parser expr: `const\s+[a-zA-Z][a-zA-Z0-9_]*\s*[=]\s*[^;]*\s*[;]`

**notes:**
  * expression part(after `=` may be composed by numbers, other constants names, `(`, `)` and operators: `+`, `-`, `*`, `/`, `%`)
  * if constant name used in expression part begins with `.`, then absolute path to constant is expected during expr evaluation
  * `.` in middle of constant name in expression is used to access child constant of parent before `.`
  * in short `.` operator is DSDL equivalent of C++ `::` operator in type scope access semantics

## Variable

```
var MY_TYPE myVariable;
```

Variable parser expr: `var\s+([.]?[a-zA-Z][a-zA-Z0-9_]*)+\s+[a-zA-Z][a-zA-Z0-9]*\s*[;]`

**notes:**

  * Variables can be used only inside Structure block
  * if type begins with `.` it mean we want to use type defined in global scope
  * `.` in middle of type is used to access child type of type before `.`
  * in short `.` operator is DSDL equivalent of C++ `::` operator in type scope access semantics

## Array

```
array MY_TYPE myArray[MY_MAX_SIZE_EXPRESSION];
```

Array parser expr: `array\s+([.]?[a-zA-Z][a-zA-Z0-9_]*)+\s+[a-zA-Z][a-zA-Z0-9]*\s*[[][^]]+[]]\s*[;]`

**notes:**

  * Arrays can be used only inside Structure block
  * if type begins with `.` it mean we want to use type defined in global scope
  * `.` in middle of type is used to access child type of type before `.`
  * in short `.` operator is DSDL equivalent of C++ `::` operator in type scope access semantics
  * Array maximal size expressions are calculated the same way as Constant expressions (part of Constant after `=`)
  * Array cannot be declared inside Option block - if you need array as Option alternative, wrap it with Structure

## Enumerator

```
enum MyEnum
{
  const ENTRY_1 = 1;
  const ENTRY_2 = 2;
  ...
  const ENTRY_N = n;
};
```

Enumerator block begin parser expr: `enum\s+[a-zA-Z][a-zA-Z0-9_]*\s*[{]`

Enumerator block end parser expr: `[}]\s*[;]`

Enumerator entry parser expr: `const\s+[a-zA-Z][a-zA-Z0-9_]*\s*[=]\s*[^;]*\s*[;]`

**note:** Enumerator entry syntax is the same as DSDL Constant, but used in diferent context -> generated code will be different than for normal Constant*

## Structure

```
struct MyStruct
{
  const SOME_CONST = 5;
  var SomeType a;
  array SomeOtherType b[1234];
  array SomeType c[SOME_CONST+SOME_OTHER_CONST];
  struct MyNastedStruct
  {
    u32 a;
    u32 b;
  };
  var MyNastedStruct d;
};
```

Structure block begin parser expr: `struct\s+[a-zA-Z][a-zA-Z0-9_]*\s*[{]`

Structure block end parser expr: `[}]\s*[;]`

**note:** Structure cannot be declared inside Enumerator block

## Option

Options is Type which define **alternative** of members - unlike Structure which define collection of members. It is like `union` in C.

```
option MyOption
{
  const SOME_CONST = 5;
  var SomeType a;
  struct MyNastedStruct
  {
    u32 a;
    u32 b;
  };
  option MyNastedOption
  {
    u32 a;
    i16 b;
  };
  var MyNastedStruct b;
  var SomeOtherType c;
  var MyNastedOption d;
};
```

Option block begin parser expr: `option\s+[a-zA-Z][a-zA-Z0-9_]*\s*[{]`

Option block end parser expr: `[}]\s*[;]`

**note:**

  * Option cannot be declared inside Enumerator block
  * Option can't have Array members.

