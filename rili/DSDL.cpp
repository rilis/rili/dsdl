#include <cstdint>
#include <list>
#include <rili/DSDL.hpp>
#include <stack>
#include <string>
#include <utility>

namespace rili {
namespace {

std::string& stringifyImpl(std::string& result, DSDL const& dsdl, std::size_t padding) {
    std::string pad(padding * 4, ' ');
    switch (dsdl.type()) {
        case DSDL::Type::Array: {
            auto const& array = dsdl.array();
            result += pad + "array " + array.type() + " " + array.name() + "[" + array.sizeExpr() + "];\n";
            break;
        }
        case DSDL::Type::Comment: {
            auto const& comment = dsdl.comment();
            result += "#" + comment + "\n";
            break;
        }
        case DSDL::Type::Const: {
            auto const& constant = dsdl.constant();
            result += pad + "const " + constant.name() + " = " + constant.expr() + ";\n";
            break;
        }
        case DSDL::Type::Enum: {
            auto const& enumerator = dsdl.enumerator();
            result += pad + "enum " + enumerator.name() + "\n" + pad + "{\n";
            for (auto const& n : enumerator.members()) {
                stringifyImpl(result, n, padding + 1);
            }
            result += pad + "};\n\n";
            break;
        }
        case DSDL::Type::Root: {
            auto const& root = dsdl.root();
            for (auto const& n : root.members()) {
                stringifyImpl(result, n, 0);
            }
            break;
        }
        case DSDL::Type::Struct: {
            auto const& structure = dsdl.structure();
            result += pad + "struct " + structure.name() + "\n" + pad + "{\n";
            for (auto const& n : structure.members()) {
                stringifyImpl(result, n, padding + 1);
            }
            result += pad + "};\n\n";
            break;
        }
        case DSDL::Type::Option: {
            auto const& option = dsdl.option();
            result += pad + "option " + option.name() + "\n" + pad + "{\n";
            for (auto const& n : option.members()) {
                stringifyImpl(result, n, padding + 1);
            }
            result += pad + "};\n\n";
            break;
        }
        case DSDL::Type::Var: {
            auto const& var = dsdl.variable();
            result += pad + "var " + var.type() + " " + var.name() + ";\n";
            break;
        }
        default: {
            //
            throw DSDL::SyntaxError(0);
        }
    }
    return result;
}

class Handler final {
 public:
    typedef std::stack<Handler, std::list<Handler>> Stack;

    enum class Type { Root, Struct, Enum, Option };
    Handler(rili::DSDL& v, Handler::Stack& s, Type type) noexcept : m_value(v), m_stack(s), m_type(type) {}
    Handler() = delete;
    Handler(Handler const&) = delete;
    Handler& operator=(Handler const&) = delete;

 public:
    void handleBlockEnd(std::size_t errorPosition);
    void handleEnumBegin(std::string const& name, std::size_t errorPosition);
    void handleStructBegin(std::string const& name, std::size_t errorPosition);
    void handleOptionBegin(std::string const& name, std::size_t errorPosition);

    void handleConst(std::string const& name, std::string const& expr);
    void handleComment(std::string const& value);
    void handleVar(std::string const& name, std::string const& type, std::size_t errorPosition);
    void handleArray(std::string const& name, std::string const& type, std::string const& sizeExpr,
                     std::size_t errorPosition);

 protected:
    rili::DSDL& m_value;
    Stack& m_stack;
    Type m_type;
};

void Handler::handleBlockEnd(std::size_t errorPosition) {
    if (m_stack.size() > 1) {
        m_stack.pop();
    } else {
        throw DSDL::SyntaxError(errorPosition);
    }
}

void Handler::handleEnumBegin(const std::string& name, std::size_t errorPosition) {
    if (m_type == Type::Root) {
        auto& v = m_value.root();
        DSDL enumerator;
        enumerator.enumerator(DSDL::EnumType(name));
        v.members().push_back(enumerator);
        m_stack.emplace(v.members().back(), m_stack, Type::Enum);
    } else if (m_type == Type::Struct) {
        auto& v = m_value.structure();
        DSDL enumerator;
        enumerator.enumerator(DSDL::EnumType(name));
        v.members().push_back(enumerator);
        m_stack.emplace(v.members().back(), m_stack, Type::Enum);
    } else if (m_type == Type::Option) {
        auto& v = m_value.option();
        DSDL enumerator;
        enumerator.enumerator(DSDL::EnumType(name));
        v.members().push_back(enumerator);
        m_stack.emplace(v.members().back(), m_stack, Type::Enum);
    } else {
        throw DSDL::SyntaxError(errorPosition);
    }
}

void Handler::handleStructBegin(const std::string& name, std::size_t errorPosition) {
    if (m_type == Type::Root) {
        auto& v = m_value.root();
        DSDL structure;
        structure.structure(DSDL::StructType(name));
        v.members().push_back(structure);
        m_stack.emplace(v.members().back(), m_stack, Type::Struct);
    } else if (m_type == Type::Struct) {
        auto& v = m_value.structure();
        DSDL structure;
        structure.structure(DSDL::StructType(name));
        v.members().push_back(structure);
        m_stack.emplace(v.members().back(), m_stack, Type::Struct);
    } else if (m_type == Type::Option) {
        auto& v = m_value.option();
        DSDL structure;
        structure.structure(DSDL::StructType(name));
        v.members().push_back(structure);
        m_stack.emplace(v.members().back(), m_stack, Type::Struct);
    } else {
        throw DSDL::SyntaxError(errorPosition);
    }
}

void Handler::handleOptionBegin(const std::string& name, std::size_t errorPosition) {
    if (m_type == Type::Root) {
        auto& v = m_value.root();
        DSDL option;
        option.option(DSDL::OptionType(name));
        v.members().push_back(option);
        m_stack.emplace(v.members().back(), m_stack, Type::Option);
    } else if (m_type == Type::Struct) {
        auto& v = m_value.structure();
        DSDL option;
        option.option(DSDL::OptionType(name));
        v.members().push_back(option);
        m_stack.emplace(v.members().back(), m_stack, Type::Option);
    } else if (m_type == Type::Option) {
        auto& v = m_value.option();
        DSDL option;
        option.option(DSDL::OptionType(name));
        v.members().push_back(option);
        m_stack.emplace(v.members().back(), m_stack, Type::Option);
    } else {
        throw DSDL::SyntaxError(errorPosition);
    }
}

void Handler::handleConst(const std::string& name, const std::string& expr) {
    if (m_type == Type::Enum) {
        auto& v = m_value.enumerator();
        DSDL constant;
        constant.constant(DSDL::ConstType(name, expr));
        v.members().push_back(constant);
    } else if (m_type == Type::Struct) {
        auto& v = m_value.structure();
        DSDL constant;
        constant.constant(DSDL::ConstType(name, expr));
        v.members().push_back(constant);
    } else if (m_type == Type::Option) {
        auto& v = m_value.option();
        DSDL constant;
        constant.constant(DSDL::ConstType(name, expr));
        v.members().push_back(constant);
    } else {
        auto& v = m_value.root();
        DSDL constant;
        constant.constant(DSDL::ConstType(name, expr));
        v.members().push_back(constant);
    }
}

void Handler::handleComment(const std::string& value) {
    if (m_type == Type::Enum) {
        auto& v = m_value.enumerator();
        DSDL comment;
        comment.comment(DSDL::CommentType(value));
        v.members().push_back(comment);
    } else if (m_type == Type::Struct) {
        auto& v = m_value.structure();
        DSDL comment;
        comment.comment(DSDL::CommentType(value));
        v.members().push_back(comment);
    } else if (m_type == Type::Option) {
        auto& v = m_value.option();
        DSDL comment;
        comment.comment(DSDL::CommentType(value));
        v.members().push_back(comment);
    } else {
        auto& v = m_value.root();
        DSDL comment;
        comment.comment(DSDL::CommentType(value));
        v.members().push_back(comment);
    }
}

void Handler::handleVar(const std::string& name, const std::string& type, std::size_t errorPosition) {
    if (m_type == Type::Struct) {
        auto& v = m_value.structure();
        DSDL var;
        var.variable(DSDL::VarType(name, type));
        v.members().push_back(var);
    } else if (m_type == Type::Option) {
        auto& v = m_value.option();
        DSDL var;
        var.variable(DSDL::VarType(name, type));
        v.members().push_back(var);
    } else {
        throw DSDL::SyntaxError(errorPosition);
    }
}

void Handler::handleArray(const std::string& name, const std::string& type, const std::string& sizeExpr,
                          std::size_t errorPosition) {
    if (m_type == Type::Struct) {
        auto& v = m_value.structure();
        DSDL array;
        array.array(DSDL::ArrayType(name, type, sizeExpr));
        v.members().push_back(array);
    } else {
        throw DSDL::SyntaxError(errorPosition);
    }
}

bool isWhitespace(char c) {
    switch (c) {
        case ' ':
        case '\r':
        case '\n':
        case '\t':
        case '\f':
        case '\v': {
            //
            return true;
        }
        default: {
            //
            return false;
        }
    }
}

bool isLetter(char c) { return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'); }
bool isDigit(char c) { return (c >= '0' && c <= '9'); }

char const* tryFetchConst(char const* position, char const* max, Handler::Stack& stack, std::size_t errorPosition) {
    if (position + 5 < max) {
        if (position[0] == 'c' && position[1] == 'o' && position[2] == 'n' && position[3] == 's' &&
            position[4] == 't') {
            if (isWhitespace(position[5])) {
                position += 5;
                while (position < max && isWhitespace(*position)) {
                    position++;
                }

                if (position < max) {
                    if (isLetter(*position)) {
                        const char* nameBegin = position;
                        while (position < max && (isLetter(*position) || isDigit(*position) || *position == '_')) {
                            position++;
                        }
                        if (position < max) {
                            const char* nameEnd = position;
                            while (position < max && isWhitespace(*position)) {
                                position++;
                            }
                            if (position + 1 < max && *position == '=') {
                                position++;
                                while (position < max && isWhitespace(*position)) {
                                    position++;
                                }
                                if (position < max) {
                                    const char* exprBegin = position;
                                    while (position < max && *position != ';') {
                                        position++;
                                    }
                                    if (position < max) {
                                        const char* exprEnd = position;
                                        while (exprEnd > exprBegin && isWhitespace(*(exprEnd - 1))) {
                                            exprEnd--;
                                        }
                                        std::string name(nameBegin, nameEnd);
                                        std::string expr(exprBegin, exprEnd);
                                        if (!name.empty() && !expr.empty()) {
                                            stack.top().handleConst(name, expr);
                                            return position + 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    throw DSDL::SyntaxError(errorPosition);
}

char const* tryFetchBlockEnd(char const* position, char const* max, Handler::Stack& stack, std::size_t errorPosition) {
    if (*position == '}') {
        position++;
        while (position < max && isWhitespace(*position)) {
            position++;
        }
        if (position < max && *position == ';') {
            stack.top().handleBlockEnd(errorPosition);
            return position + 1;
        }
    }
    throw DSDL::SyntaxError(errorPosition);
}

char const* tryFetchEnumBegin(char const* position, char const* max, Handler::Stack& stack, std::size_t errorPosition) {
    if (position + 4 < max) {
        if (position[0] == 'e' && position[1] == 'n' && position[2] == 'u' && position[3] == 'm') {
            if (isWhitespace(position[4])) {
                position += 4;
                while (position < max && isWhitespace(*position)) {
                    position++;
                }
                if (isLetter(*position)) {
                    const char* nameBegin = position;
                    while (position < max && (isLetter(*position) || isDigit(*position) || *position == '_')) {
                        position++;
                    }
                    if (position < max && (isWhitespace(*position) || *position == '{')) {
                        const char* nameEnd = position;
                        while (position < max && isWhitespace(*position)) {
                            position++;
                        }
                        if (position < max) {
                            while (position < max && *position != '{') {
                                position++;
                            }
                            if (position < max) {
                                std::string name(nameBegin, nameEnd);
                                if (!name.empty()) {
                                    stack.top().handleEnumBegin(name, errorPosition);
                                    return position + 1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    throw DSDL::SyntaxError(errorPosition);
}

char const* tryFetchStructBegin(char const* position, char const* max, Handler::Stack& stack,
                                std::size_t errorPosition) {
    if (position + 6 < max) {
        if (position[0] == 's' && position[1] == 't' && position[2] == 'r' && position[3] == 'u' &&
            position[4] == 'c' && position[5] == 't') {
            if (isWhitespace(position[6])) {
                position += 6;
                while (position < max && isWhitespace(*position)) {
                    position++;
                }
                if (isLetter(*position)) {
                    const char* nameBegin = position;
                    while (position < max && (isLetter(*position) || isDigit(*position) || *position == '_')) {
                        position++;
                    }
                    if (position < max && (isWhitespace(*position) || *position == '{')) {
                        const char* nameEnd = position;
                        while (position < max && isWhitespace(*position)) {
                            position++;
                        }
                        if (position < max) {
                            while (position < max && *position != '{') {
                                position++;
                            }
                            if (position < max) {
                                std::string name(nameBegin, nameEnd);
                                if (!name.empty()) {
                                    stack.top().handleStructBegin(name, errorPosition);
                                    return position + 1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    throw DSDL::SyntaxError(errorPosition);
}

char const* tryFetchOptionBegin(char const* position, char const* max, Handler::Stack& stack,
                                std::size_t errorPosition) {
    if (position + 6 < max) {
        if (position[0] == 'o' && position[1] == 'p' && position[2] == 't' && position[3] == 'i' &&
            position[4] == 'o' && position[5] == 'n') {
            if (isWhitespace(position[6])) {
                position += 6;
                while (position < max && isWhitespace(*position)) {
                    position++;
                }
                if (isLetter(*position)) {
                    const char* nameBegin = position;
                    while (position < max && (isLetter(*position) || isDigit(*position) || *position == '_')) {
                        position++;
                    }
                    if (position < max && (isWhitespace(*position) || *position == '{')) {
                        const char* nameEnd = position;
                        while (position < max && isWhitespace(*position)) {
                            position++;
                        }
                        if (position < max) {
                            while (position < max && *position != '{') {
                                position++;
                            }
                            if (position < max) {
                                std::string name(nameBegin, nameEnd);
                                if (!name.empty()) {
                                    stack.top().handleOptionBegin(name, errorPosition);
                                    return position + 1;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    throw DSDL::SyntaxError(errorPosition);
}

char const* tryFetchArray(char const* position, char const* max, Handler::Stack& stack, std::size_t errorPosition) {
    if (position + 5 < max) {
        if (position[0] == 'a' && position[1] == 'r' && position[2] == 'r' && position[3] == 'a' &&
            position[4] == 'y') {
            if (isWhitespace(position[5])) {
                position += 5;
                while (position < max && isWhitespace(*position)) {
                    position++;
                }
                if (position < max) {
                    if (isLetter(*position) || *position == '.') {
                        if (*position == '.') {
                            if (!(position + 1 < max && isLetter(*(position + 1)))) {
                                throw DSDL::SyntaxError(errorPosition);
                            }
                        }
                        const char* typeBegin = position;
                        while (position < max &&
                               (isLetter(*position) || isDigit(*position) || *position == '_' || *position == '.')) {
                            if (*position == '.') {
                                if (!(position + 1 < max && isLetter(*(position + 1)))) {
                                    throw DSDL::SyntaxError(errorPosition);
                                }
                            }
                            position++;
                        }
                        if (position < max) {
                            const char* typeEnd = position;
                            while (position < max && isWhitespace(*position)) {
                                position++;
                            }
                            if (position < max) {
                                if (isLetter(*position)) {
                                    const char* nameBegin = position;
                                    while (position < max &&
                                           (isLetter(*position) || isDigit(*position) || *position == '_')) {
                                        position++;
                                    }
                                    if (position < max) {
                                        const char* nameEnd = position;
                                        while (position < max && isWhitespace(*position)) {
                                            position++;
                                        }
                                        if (position + 1 < max) {
                                            if (*position == '[') {
                                                position++;
                                                while (position < max && isWhitespace(*position)) {
                                                    position++;
                                                }
                                                char const* exprBegin = position;
                                                while (position < max && *position != ']') {
                                                    position++;
                                                }
                                                if (position < max) {
                                                    char const* exprEnd = position;
                                                    position++;
                                                    while (exprEnd > exprBegin && isWhitespace(*(exprEnd - 1))) {
                                                        exprEnd--;
                                                    }
                                                    while (position < max && isWhitespace(*position)) {
                                                        position++;
                                                    }
                                                    if (position < max && *position == ';') {
                                                        std::string name(nameBegin, nameEnd);
                                                        std::string type(typeBegin, typeEnd);
                                                        std::string sizeExpr(exprBegin, exprEnd);
                                                        stack.top().handleArray(name, type, sizeExpr, errorPosition);
                                                        return position + 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    throw DSDL::SyntaxError(errorPosition);
}

char const* tryFetchVar(char const* position, char const* max, Handler::Stack& stack, std::size_t errorPosition) {
    if (position + 3 < max) {
        if (position[0] == 'v' && position[1] == 'a' && position[2] == 'r') {
            if (isWhitespace(position[3])) {
                position += 3;
                while (position < max && isWhitespace(*position)) {
                    position++;
                }
                if (position < max) {
                    if (isLetter(*position) || *position == '.') {
                        if (*position == '.') {
                            if (!(position + 1 < max && isLetter(*(position + 1)))) {
                                throw DSDL::SyntaxError(errorPosition);
                            }
                        }
                        const char* typeBegin = position;
                        while (position < max &&
                               (isLetter(*position) || isDigit(*position) || *position == '_' || *position == '.')) {
                            if (*position == '.') {
                                if (!(position + 1 < max && isLetter(*(position + 1)))) {
                                    throw DSDL::SyntaxError(errorPosition);
                                }
                            }
                            position++;
                        }
                        if (position < max) {
                            const char* typeEnd = position;
                            while (position < max && isWhitespace(*position)) {
                                position++;
                            }
                            if (position < max) {
                                if (isLetter(*position)) {
                                    const char* nameBegin = position;
                                    while (position < max &&
                                           (isLetter(*position) || isDigit(*position) || *position == '_')) {
                                        position++;
                                    }
                                    if (position < max) {
                                        const char* nameEnd = position;
                                        while (position < max && isWhitespace(*position)) {
                                            position++;
                                        }
                                        if (position < max) {
                                            while (position < max && isWhitespace(*position)) {
                                                position++;
                                            }
                                            if (position < max && *position == ';') {
                                                std::string type(typeBegin, typeEnd);
                                                std::string name(nameBegin, nameEnd);
                                                if (!type.empty() && !name.empty()) {
                                                    stack.top().handleVar(name, type, errorPosition);
                                                    return position + 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    throw DSDL::SyntaxError(errorPosition);
}

char const* tryFetchComment(char const* position, char const* max, Handler::Stack& stack, std::size_t errorPosition) {
    if (*position == '#') {
        position++;
        char const* begin = position;
        while (position < max && (*position != '\r' && *position != '\n')) {
            position++;
        }
        stack.top().handleComment(std::string(begin, position));
        return position;
    }
    throw DSDL::SyntaxError(errorPosition);
}

void reader(Handler::Stack& stack, const std::string& src) {
    char const* const startPosition = src.c_str();
    char const* position = src.c_str();
    char const* max = position + src.size();
    std::size_t errorPosition = 0;

    while (position < max) {
        errorPosition = static_cast<size_t>(position - startPosition);
        switch (*position) {
            case ' ':
            case '\r':
            case '\n':
            case '\t':
            case '\f':
            case '\v': {
                position++;
                break;
            }
            case 'e': {
                position = tryFetchEnumBegin(position, max, stack, errorPosition);
                break;
            }
            case 'o': {
                position = tryFetchOptionBegin(position, max, stack, errorPosition);
                break;
            }
            case 's': {
                position = tryFetchStructBegin(position, max, stack, errorPosition);
                break;
            }
            case '}': {
                position = tryFetchBlockEnd(position, max, stack, errorPosition);
                break;
            }
            case 'a': {
                position = tryFetchArray(position, max, stack, errorPosition);
                break;
            }
            case 'c': {
                position = tryFetchConst(position, max, stack, errorPosition);
                break;
            }
            case 'v': {
                position = tryFetchVar(position, max, stack, errorPosition);
                break;
            }
            case '#': {
                position = tryFetchComment(position, max, stack, errorPosition);
                break;
            }
            default: {
                //
                throw DSDL::SyntaxError(errorPosition);
            }
        }
    }

    if (stack.size() != 1) {
        throw DSDL::SyntaxError(0);
    }
}

}  // namespace
DSDL::SyntaxError::SyntaxError(size_t position) noexcept
    : std::exception(),      //
      m_position(position),  //
      m_what("rili::DSDL::SyntaxError at " + std::to_string(m_position)) {}

const char* DSDL::SyntaxError::what() const noexcept { return m_what.c_str(); }

size_t DSDL::SyntaxError::position() const noexcept { return m_position; }

void DSDL::parse(const std::string& dsdl) {
    m_storage.set<RootType>(RootType{});  // NOLINT
    m_type = Type::Root;

    Handler::Stack stack;
    stack.emplace(*this, stack, Handler::Type::Root);
    reader(stack, dsdl);
}

std::string DSDL::stringify() const noexcept {
    std::string result;
    return stringifyImpl(result, *this, 0);
}

void DSDL::Traverser::run(const DSDL::RootType& dsdl) {
    emitRootBegin();
    for (auto const& member : dsdl.members()) {
        auto type = member.type();
        if (type != DSDL::Type::Array && type != DSDL::Type::Var) {
            run(member);
        } else {
            throw DSDL::TraverserError(std::string("Array and Var must have parent!"));
        }
    }
    emitRootEnd();
}

void DSDL::Traverser::run(const DSDL& dsdl) {
    switch (dsdl.type()) {
        case DSDL::Type::Array: {
            auto const& array = dsdl.array();
            auto v = evalExpression(array.sizeExpr());
            if (v >= 0) {
                emitArray(array.name(), array.type(), static_cast<std::uint64_t>(v));
            } else {
                throw DSDL::TraverserError("Evaluated maximal size of array less than 0!");
            }
            break;
        }
        case DSDL::Type::Comment: {
            auto const& comment = dsdl.comment();
            emitComment(comment);
            break;
        }
        case DSDL::Type::Const: {
            auto const& constant = dsdl.constant();
            auto v = evalExpression(constant.expr());
            storeConstantValue(parentNames(), constant.name(), v);
            emitConstant(constant.name(), v);
            break;
        }
        case DSDL::Type::Enum: {
            auto const& enumerator = dsdl.enumerator();
            m_parentNames.emplace_back(enumerator.name());
            emitEnumeratorBegin(enumerator.name());
            for (auto const& n : enumerator.members()) {
                if (n.type() == DSDL::Type::Const) {
                    auto const& entry = n.constant();
                    emitEnumeratorEntry(entry.name(), evalExpression(entry.expr()));
                } else if (n.type() == DSDL::Type::Comment) {
                    run(n);
                } else {
                    throw DSDL::TraverserError("Invalid Enum entry type!");
                }
            }
            emitEnumeratorEnd(enumerator.name());
            m_parentNames.pop_back();
            break;
        }
        case DSDL::Type::Struct: {
            auto const& structure = dsdl.structure();
            m_parentNames.emplace_back(structure.name());
            emitStructureBegin(structure.name());
            for (auto const& n : structure.members()) {
                run(n);
            }
            emitStructureEnd(structure.name());
            m_parentNames.pop_back();
            break;
        }
        case DSDL::Type::Option: {
            auto const& option = dsdl.option();
            m_parentNames.push_back(option.name());
            emitOptionBegin(option.name());

            for (auto const& n : option.members()) {
                if (n.type() != DSDL::Type::Var) {
                    run(n);
                }
            }

            std::list<std::pair<std::string, std::string>> alternatives;
            for (auto const& n : option.members()) {
                if (n.type() == DSDL::Type::Var) {
                    auto v = n.variable();
                    alternatives.push_back({v.type(), v.name()});
                }
            }

            emitOptionAlternatives(alternatives);
            emitOptionEnd(option.name());
            m_parentNames.pop_back();
            break;
        }
        case DSDL::Type::Var: {
            auto const& var = dsdl.variable();
            emitVariable(var.name(), var.type());
            break;
        }
        default: {
            //
            throw DSDL::TraverserError("Internal error!");
        }
    }
}

const std::list<std::string>& DSDL::Traverser::parentNames() const { return m_parentNames; }

void DSDL::Traverser::storeConstantValue(std::list<std::string> const& parents, const std::string& localName,
                                         std::int64_t value) {
    auto const uniqueName = createConstantUniqueName(parents, localName);
    if (m_constants.find(uniqueName) != m_constants.end()) {
        throw DSDL::TraverserError("Constant " + uniqueName + " cannot be redefined!");
    } else {
        m_constants.emplace(uniqueName, value);
    }
}

std::string DSDL::Traverser::createConstantUniqueName(const std::list<std::string>& parents,
                                                      const std::string& localName) const {
    if (localName[0] == '.') {
        return localName;
    } else {
        std::string uniqueName;
        for (auto const& e : parents) {
            uniqueName += "." + e;
        }
        uniqueName += "." + localName;
        return uniqueName;
    }
}

std::list<DSDL::Traverser::ExprToken> DSDL::Traverser::getExprTokens(const std::string& expr) const {
    std::list<DSDL::Traverser::ExprToken> result;
    for (const auto* p = expr.c_str(); *p != '\0'; p++) {
        switch (*p) {
            case '+': {
                result.push_back({DSDL::Traverser::ExprToken::Type::Operator, "+", 2});
                break;
            }
            case '-': {
                result.push_back({DSDL::Traverser::ExprToken::Type::Operator, "-", 2});
                break;
            }
            case '*': {
                result.push_back({DSDL::Traverser::ExprToken::Type::Operator, "*", 3});
                break;
            }
            case '/': {
                result.push_back({DSDL::Traverser::ExprToken::Type::Operator, "/", 3});
                break;
            }
            case '%': {
                result.push_back({DSDL::Traverser::ExprToken::Type::Operator, "%", 3});
                break;
            }
            case '(': {
                result.push_back({DSDL::Traverser::ExprToken::Type::LeftParen, "(", 0});
                break;
            }
            case ')': {
                result.push_back({DSDL::Traverser::ExprToken::Type::RightParen, ")", 0});
                break;
            }
            default: {
                if ((*p >= '0' && *p <= '9') || *p == '-') {
                    std::string number;
                    number += *p;
                    p++;
                    while (*p >= '0' && *p <= '9') {
                        number += *p;
                        p++;
                    }
                    result.push_back({DSDL::Traverser::ExprToken::Type::Value, number, 0});
                    p--;
                } else if ((*p >= 'a' && *p <= 'z') || (*p >= 'A' && *p <= 'Z') || *p == '.' || *p == '_') {
                    std::string var;
                    var += *p;
                    p++;
                    while ((*p >= '0' && *p <= '9') || (*p >= 'a' && *p <= 'z') || (*p >= 'A' && *p <= 'Z') ||
                           *p == '.' || *p == '_') {
                        var += *p;
                        p++;
                    }
                    result.push_back({DSDL::Traverser::ExprToken::Type::Value, getExprConstValue(var), 0});
                    p--;
                }
                break;
            }
        }
    }
    return result;
}

std::list<DSDL::Traverser::ExprToken> DSDL::Traverser::shuntingYard(
    const std::list<DSDL::Traverser::ExprToken>& tokens) const {
    std::list<DSDL::Traverser::ExprToken> queue;
    std::list<DSDL::Traverser::ExprToken> stack;

    for (auto token : tokens) {
        switch (token.type) {
            case DSDL::Traverser::ExprToken::Type::Value: {
                queue.push_back(token);
                break;
            }
            case DSDL::Traverser::ExprToken::Type::Operator: {
                const auto o1 = token;
                while (!stack.empty()) {
                    const auto o2 = stack.back();
                    if (o1.precedence <= o2.precedence) {
                        stack.pop_back();
                        queue.push_back(o2);

                        continue;
                    }
                    break;
                }
                stack.push_back(o1);
                break;
            }

            case DSDL::Traverser::ExprToken::Type::LeftParen: {
                stack.push_back(token);
                break;
            }

            case DSDL::Traverser::ExprToken::Type::RightParen: {
                bool match = false;
                while (!stack.empty()) {
                    const auto tos = stack.back();
                    if (tos.type != DSDL::Traverser::ExprToken::Type::LeftParen) {
                        stack.pop_back();
                        queue.push_back(tos);
                    }
                    stack.pop_back();
                    match = true;
                    break;
                }

                if (!match && stack.empty()) {
                    throw DSDL::TraverserError("Invalid Constant value expression!");
                }
                break;
            }
        }
    }

    while (!stack.empty()) {
        if (stack.back().type == DSDL::Traverser::ExprToken::Type::LeftParen) {
            throw DSDL::TraverserError("Invalid Constant value expression!");
        }

        queue.push_back(std::move(stack.back()));
        stack.pop_back();
    }

    return queue;
}

std::int64_t DSDL::Traverser::evalReversePolishNotation(const std::list<DSDL::Traverser::ExprToken>& tokens) const {
    std::list<std::int64_t> stack;
    for (auto const& t : tokens) {
        if (t.type == DSDL::Traverser::ExprToken::Type::Value) {
            stack.push_back(std::stoll(t.data));
        } else {
            if (stack.size() < 2) {
                throw DSDL::TraverserError("Invalid Constant value expression!");
            }
            std::int64_t op2 = stack.back();
            stack.pop_back();
            std::int64_t op1 = stack.back();
            stack.pop_back();

            std::int64_t result;
            if (t.data == "+") {
                result = op1 + op2;
            } else if (t.data == "-") {
                result = op1 - op2;
            } else if (t.data == "*") {
                result = op1 * op2;
            } else if (t.data == "/") {
                result = op1 / op2;
            } else if (t.data == "%") {
                result = op1 % op2;
            } else {
                throw DSDL::TraverserError("Invalid Constant value expression!");
            }
            stack.push_back(result);
        }
    }

    if (stack.size() != 1) {
        throw DSDL::TraverserError("Invalid Constant value expression!");
    } else {
        return stack.back();
    }
}

std::string DSDL::Traverser::getExprConstValue(const std::string& constName) const {
    auto parents = parentNames();
    while (true) {
        auto candidate = createConstantUniqueName(parents, constName);
        auto found = m_constants.find(candidate);
        if (found != m_constants.end()) {
            return std::to_string(found->second);
        }
        if (!parents.empty()) {
            parents.pop_back();
        } else {
            break;
        }
    }
    throw DSDL::TraverserError("Constant name(" + constName + ") used in expression is not defined!");
}

std::int64_t DSDL::Traverser::evalExpression(const std::string& expr) const {
    return evalReversePolishNotation(shuntingYard(getExprTokens(expr)));
}

DSDL::TraverserError::TraverserError(const std::string& what) : m_what("rili::DSDL::TraverserError: " + what) {}

const char* DSDL::TraverserError::what() const noexcept { return m_what.c_str(); }

}  // namespace rili
