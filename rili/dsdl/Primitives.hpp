#pragma once
#include <cstdint>
#include <rili/Endian.hpp>
#include <type_traits>

namespace rili {
namespace dsdl {

typedef std::uint8_t u8;
static_assert(sizeof(u8) == 1, "u8 size must be 8 bit");
static_assert(std::is_same<u8, unsigned char>::value, "u8 must be an alias for unsigned char");

typedef std::uint16_t u16;
static_assert(sizeof(u16) == 2, "u16 size must be 16 bit");

typedef std::uint32_t u32;
static_assert(sizeof(u32) == 4, "u32 size must be 32 bit");

typedef std::uint64_t u64;
static_assert(sizeof(u64) == 8, "u64 size must be 64 bit");

typedef std::int8_t i8;
static_assert(sizeof(i8) == 1, "i8 size must be 8 bit");

typedef std::int16_t i16;
static_assert(sizeof(i16) == 2, "i16 size must be 16 bit");

typedef std::int32_t i32;
static_assert(sizeof(i32) == 4, "i32 size must be 32 bit");

typedef std::int64_t i64;
static_assert(sizeof(i64) == 8, "i64 size must be 64 bit");

/**
 * @brief The Buffer class is abstract interface for byte collection used to serialize/ deserialize DSDL data structures
 */
class Buffer {
 protected:
    Buffer() = default;
    ~Buffer() = default;

 public:
    /**
     * @brief push used by serialize methods to push at the end of buffer single e byte
     * @param e
     * @return true if success, false othrewise
     */
    virtual bool push(u8 e) = 0;
    /**
     * @brief pop used by deserialize methods to pop from front of buffer single byte
     * @param e
     * @return true if success, false othrewise
     */
    virtual bool pop(u8 &e) = 0;
};

inline bool serialize(u8 v, Buffer &buffer) { return buffer.push(v); }
inline bool serialize(u16 v, Buffer &buffer) {
    auto const u = rili::endian::hostToLittle(v);
    auto const *b = reinterpret_cast<u8 const *>(&u);
    return buffer.push(b[0]) && buffer.push(b[1]);
}

inline bool serialize(u32 v, Buffer &buffer) {
    auto const u = rili::endian::hostToLittle(v);
    auto const *b = reinterpret_cast<u8 const *>(&u);
    return buffer.push(b[0]) && buffer.push(b[1]) && buffer.push(b[2]) && buffer.push(b[3]);
}

inline bool serialize(u64 v, Buffer &buffer) {
    auto const u = rili::endian::hostToLittle(v);
    auto const *b = reinterpret_cast<u8 const *>(&u);
    return buffer.push(b[0]) && buffer.push(b[1]) && buffer.push(b[2]) && buffer.push(b[3]) && buffer.push(b[4]) &&
           buffer.push(b[5]) && buffer.push(b[6]) && buffer.push(b[7]);
}

inline bool serialize(const i8 &v, Buffer &buffer) { return serialize(*reinterpret_cast<u8 const *>(&v), buffer); }
inline bool serialize(const i16 &v, Buffer &buffer) { return serialize(*reinterpret_cast<u16 const *>(&v), buffer); }
inline bool serialize(const i32 &v, Buffer &buffer) { return serialize(*reinterpret_cast<u32 const *>(&v), buffer); }
inline bool serialize(const i64 &v, Buffer &buffer) { return serialize(*reinterpret_cast<u64 const *>(&v), buffer); }
inline bool deserialize(u8 &v, Buffer &buffer) { return buffer.pop(v); }
inline bool deserialize(u16 &v, Buffer &buffer) {
    u16 u;
    auto *b = reinterpret_cast<u8 *>(&u);
    if (buffer.pop(b[0]) && buffer.pop(b[1])) {
        v = rili::endian::littleToHost(u);
        return true;
    } else {
        return false;
    }
}

inline bool deserialize(u32 &v, Buffer &buffer) {
    u32 u;
    auto *b = reinterpret_cast<u8 *>(&u);
    if (buffer.pop(b[0]) && buffer.pop(b[1]) && buffer.pop(b[2]) && buffer.pop(b[3])) {
        v = rili::endian::littleToHost(u);
        return true;
    } else {
        return false;
    }
}

inline bool deserialize(u64 &v, Buffer &buffer) {
    u64 u;
    auto *b = reinterpret_cast<u8 *>(&u);
    if (buffer.pop(b[0]) && buffer.pop(b[1]) && buffer.pop(b[2]) && buffer.pop(b[3]) && buffer.pop(b[4]) &&
        buffer.pop(b[5]) && buffer.pop(b[6]) && buffer.pop(b[7])) {
        v = rili::endian::littleToHost(u);
        return true;
    } else {
        return false;
    }
}

inline bool deserialize(i8 &v, Buffer &buffer) { return deserialize(*reinterpret_cast<u8 *>(&v), buffer); }
inline bool deserialize(i16 &v, Buffer &buffer) { return deserialize(*reinterpret_cast<u16 *>(&v), buffer); }
inline bool deserialize(i32 &v, Buffer &buffer) { return deserialize(*reinterpret_cast<u32 *>(&v), buffer); }
inline bool deserialize(i64 &v, Buffer &buffer) { return deserialize(*reinterpret_cast<u64 *>(&v), buffer); }
}  // namespace dsdl
}  // namespace rili
