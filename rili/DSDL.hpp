#pragma once
/** @file */

#include <cstdint>
#include <exception>
#include <list>
#include <map>
#include <rili/Variant.hpp>
#include <string>
#include <utility>

namespace rili {
/**
 * @brief The DSDL class
 */
class DSDL final {
 public:
    /**
     * @brief The SyntaxError class
     */
    class SyntaxError final : public std::exception {
     public:
        /**
         * @brief SyntaxError
         * @param position
         */
        explicit SyntaxError(size_t position) noexcept;

        /**
         * @brief what
         * @return
         */
        const char* what() const noexcept override;

        /**
         * @brief position
         * @return
         */
        size_t position() const noexcept;

        virtual ~SyntaxError() = default;

        /* @cond INTERNAL */
        SyntaxError(SyntaxError const& other) = default;
        SyntaxError& operator=(SyntaxError const& other) = default;
        /* @endcond INTERNAL */

     private:
        SyntaxError() = delete;
        size_t m_position;
        std::string m_what;
    };

    /**
     * @brief The TraverserError class
     */
    class TraverserError final : public std::exception {
     public:
        /**
         * @brief TraverserError
         * @param what
         */
        explicit TraverserError(std::string const& what);

        /**
         * @brief what
         * @return
         */
        const char* what() const noexcept override;

        virtual ~TraverserError() = default;

        /* @cond INTERNAL */
        TraverserError(TraverserError const& other) = default;
        TraverserError& operator=(TraverserError const& other) = default;
        /* @endcond INTERNAL */

     private:
        TraverserError() = delete;
        std::string m_what;
    };

    /**
     * @brief CommentType
     */
    typedef std::string CommentType;

    /**
     * @brief The RootType class
     */
    class RootType final {
     public:
        /**
         * @brief Members
         */
        typedef std::list<DSDL> Members;

     public:
        /**
         * @brief members
         * @return
         */
        Members& members() noexcept { return m_members; }

        /**
         * @brief members
         * @return
         */
        Members const& members() const noexcept { return m_members; }

     public:
        /**
         * @brief RootType
         */
        RootType() : m_members() {}

        /**
         * @brief RootType
         * @param members
         */
        explicit RootType(Members const& members) : m_members(members) {}

        /**
         * @brief RootType
         * @param other
         */
        RootType(RootType const& other) = default;

        /**
         * @brief operator=
         * @param other
         * @return
         */
        RootType& operator=(RootType const& other) = default;

     private:
        Members m_members;
    };

    /**
     * @brief The OptionType class
     */
    class OptionType final {
     public:
        /**
         * @brief Members
         */
        typedef std::list<DSDL> Members;

     public:
        /**
         * @brief name
         * @return
         */
        std::string& name() { return m_name; }

        /**
         * @brief name
         * @return
         */
        std::string const& name() const { return m_name; }

        /**
         * @brief members
         * @return
         */
        Members& members() noexcept { return m_members; }

        /**
         * @brief members
         * @return
         */
        Members const& members() const noexcept { return m_members; }

     public:
        OptionType() = delete;

        /**
         * @brief OptionType
         * @param name
         */
        explicit OptionType(std::string const& name) : m_name(name), m_members() {}
        /**
         * @brief OptionType
         * @param name
         * @param members
         */
        OptionType(std::string const& name, Members const& members) : m_name(name), m_members(members) {}

        /**
         * @brief OptionType
         * @param other
         */
        OptionType(OptionType const& other) = default;

        /**
         * @brief operator=
         * @param other
         * @return
         */
        OptionType& operator=(OptionType const& other) = default;

     private:
        std::string m_name;
        Members m_members;
    };

    /**
     * @brief The StructType class
     */
    class StructType final {
     public:
        /**
         * @brief Members
         */
        typedef std::list<DSDL> Members;

     public:
        /**
         * @brief name
         * @return
         */
        std::string& name() { return m_name; }

        /**
         * @brief name
         * @return
         */
        std::string const& name() const { return m_name; }

        /**
         * @brief members
         * @return
         */
        Members& members() noexcept { return m_members; }

        /**
         * @brief members
         * @return
         */
        Members const& members() const noexcept { return m_members; }

     public:
        StructType() = delete;

        /**
         * @brief StructType
         * @param name
         */
        explicit StructType(std::string const& name) : m_name(name), m_members() {}
        /**
         * @brief StructType
         * @param name
         * @param members
         */
        StructType(std::string const& name, Members const& members) : m_name(name), m_members(members) {}

        /**
         * @brief StructType
         * @param other
         */
        StructType(StructType const& other) = default;

        /**
         * @brief operator=
         * @param other
         * @return
         */
        StructType& operator=(StructType const& other) = default;

     private:
        std::string m_name;
        Members m_members;
    };

    /**
     * @brief The EnumType class
     */
    class EnumType final {
     public:
        /**
         * @brief Members
         */
        typedef std::list<DSDL> Members;

     public:
        /**
         * @brief name
         * @return
         */
        std::string& name() { return m_name; }

        /**
         * @brief name
         * @return
         */
        std::string const& name() const { return m_name; }

        /**
         * @brief members
         * @return
         */
        Members& members() noexcept { return m_members; }

        /**
         * @brief members
         * @return
         */
        Members const& members() const noexcept { return m_members; }

     public:
        EnumType() = delete;
        /**
         * @brief EnumType
         * @param name
         */
        explicit EnumType(std::string const& name) : m_name(name), m_members() {}

        /**
         * @brief EnumType
         * @param name
         * @param members
         */
        EnumType(std::string const& name, Members const& members) : m_name(name), m_members(members) {}

        /**
         * @brief EnumType
         * @param other
         */
        EnumType(EnumType const& other) = default;

        /**
         * @brief operator=
         * @param other
         * @return
         */
        EnumType& operator=(EnumType const& other) = default;

     private:
        std::string m_name;
        Members m_members;
    };

    /**
     * @brief The VarType class
     */
    class VarType final {
     public:
        /**
         * @brief VarType
         * @param name
         * @param type
         */
        VarType(std::string const& name, std::string const& type) : m_type(type), m_name(name) {}

        /**
         * @brief VarType
         * @param other
         */
        VarType(VarType const& other) = default;

        /**
         * @brief operator=
         * @param other
         * @return
         */
        VarType& operator=(VarType const& other) = default;

        /**
         * @brief name
         * @return
         */
        std::string& name() { return m_name; }

        /**
         * @brief name
         * @return
         */
        std::string const& name() const { return m_name; }

        /**
         * @brief type
         * @return
         */
        std::string& type() { return m_type; }

        /**
         * @brief type
         * @return
         */
        std::string const& type() const { return m_type; }

     private:
        VarType() = delete;

     private:
        std::string m_type;
        std::string m_name;
    };

    /**
     * @brief The ConstType class
     */
    class ConstType final {
     public:
        /**
         * @brief ConstType
         * @param name
         * @param expr
         */
        ConstType(std::string const& name, std::string const& expr) : m_name(name), m_expr(expr) {}

        /**
         * @brief ConstType
         * @param other
         */
        ConstType(ConstType const& other) = default;

        /**
         * @brief operator=
         * @param other
         * @return
         */
        ConstType& operator=(ConstType const& other) = default;

        /**
         * @brief name
         * @return
         */
        std::string& name() { return m_name; }

        /**
         * @brief name
         * @return
         */
        std::string const& name() const { return m_name; }

        /**
         * @brief expr
         * @return
         */
        std::string& expr() { return m_expr; }

        /**
         * @brief expr
         * @return
         */
        std::string const& expr() const { return m_expr; }

     private:
        ConstType() = delete;

     private:
        std::string m_name;
        std::string m_expr;
    };

    /**
     * @brief The ArrayType class
     */
    class ArrayType final {
     public:
        /**
         * @brief ArrayType
         * @param name
         * @param type
         * @param sizeExpr
         */
        ArrayType(std::string const& name, std::string const& type, std::string const& sizeExpr)
            : m_name(name), m_type(type), m_sizeExpr(sizeExpr) {}

        /**
         * @brief ArrayType
         * @param other
         */
        ArrayType(ArrayType const& other) = default;

        /**
         * @brief operator=
         * @param other
         * @return
         */
        ArrayType& operator=(ArrayType const& other) = default;

        /**
         * @brief name
         * @return
         */
        std::string& name() { return m_name; }

        /**
         * @brief name
         * @return
         */
        std::string const& name() const { return m_name; }

        /**
         * @brief type
         * @return
         */
        std::string& type() { return m_type; }

        /**
         * @brief type
         * @return
         */
        std::string const& type() const { return m_type; }

        /**
         * @brief sizeExpr
         * @return
         */
        std::string& sizeExpr() { return m_sizeExpr; }

        /**
         * @brief sizeExpr
         * @return
         */
        std::string const& sizeExpr() const { return m_sizeExpr; }

     private:
        ArrayType() = delete;

     private:
        std::string m_name;
        std::string m_type;
        std::string m_sizeExpr;
    };

    /**
     * @brief The Type enum
     */
    enum class Type { Root, Array, Const, Enum, Var, Struct, Comment, Option };
    /**
     * @brief type
     * @return
     */
    Type type() const noexcept { return m_type; }

    /**
     * @brief root
     * @return
     */
    RootType& root() {
        if (m_type != Type::Root) {
            throw std::bad_cast();
        }
        return m_storage.get<RootType>();
    }
    /**
     * @brief root
     * @return
     */
    RootType const& root() const {
        if (m_type != Type::Root) {
            throw std::bad_cast();
        }
        return m_storage.get<RootType>();
    }

    /**
     * @brief root
     * @param t
     */
    void root(RootType const& t) {
        m_type = Type::Root;
        m_storage.set<RootType>(t);
    }

    /**
     * @brief option
     * @return
     */
    OptionType& option() {
        if (m_type != Type::Option) {
            throw std::bad_cast();
        }
        return m_storage.get<OptionType>();
    }
    /**
     * @brief option
     * @return
     */
    OptionType const& option() const {
        if (m_type != Type::Option) {
            throw std::bad_cast();
        }
        return m_storage.get<OptionType>();
    }

    /**
     * @brief option
     * @param t
     */
    void option(OptionType const& t) {
        m_type = Type::Option;
        m_storage.set<OptionType>(t);
    }

    /**
     * @brief comment
     * @return
     */
    CommentType& comment() {
        if (m_type != Type::Comment) {
            throw std::bad_cast();
        }
        return m_storage.get<CommentType>();
    }
    /**
     * @brief comment
     * @return
     */
    CommentType const& comment() const {
        if (m_type != Type::Comment) {
            throw std::bad_cast();
        }
        return m_storage.get<CommentType>();
    }

    /**
     * @brief comment
     * @param t
     */
    void comment(CommentType const& t) {
        m_type = Type::Comment;
        m_storage.set<CommentType>(t);
    }

    /**
     * @brief array
     * @return
     */
    ArrayType& array() {
        if (m_type != Type::Array) {
            throw std::bad_cast();
        }
        return m_storage.get<ArrayType>();
    }

    /**
     * @brief array
     * @return
     */
    ArrayType const& array() const {
        if (m_type != Type::Array) {
            throw std::bad_cast();
        }
        return m_storage.get<ArrayType>();
    }

    /**
     * @brief array
     * @param t
     */
    void array(ArrayType const& t) {
        m_type = Type::Array;
        m_storage.set<ArrayType>(t);
    }

    /**
     * @brief structure
     * @return
     */
    StructType& structure() {
        if (m_type != Type::Struct) {
            throw std::bad_cast();
        }
        return m_storage.get<StructType>();
    }

    /**
     * @brief structure
     * @return
     */
    StructType const& structure() const {
        if (m_type != Type::Struct) {
            throw std::bad_cast();
        }
        return m_storage.get<StructType>();
    }

    /**
     * @brief structure
     * @param t
     */
    void structure(StructType const& t) {
        m_type = Type::Struct;
        m_storage.set<StructType>(t);
    }

    /**
     * @brief enumerator
     * @return
     */
    EnumType& enumerator() {
        if (m_type != Type::Enum) {
            throw std::bad_cast();
        }
        return m_storage.get<EnumType>();
    }

    /**
     * @brief enumerator
     * @return
     */
    EnumType const& enumerator() const {
        if (m_type != Type::Enum) {
            throw std::bad_cast();
        }
        return m_storage.get<EnumType>();
    }

    /**
     * @brief enumerator
     * @param t
     */
    void enumerator(EnumType const& t) {
        m_type = Type::Enum;
        m_storage.set<EnumType>(t);
    }

    /**
     * @brief constant
     * @return
     */
    ConstType& constant() {
        if (m_type != Type::Const) {
            throw std::bad_cast();
        }
        return m_storage.get<ConstType>();
    }

    /**
     * @brief constant
     * @return
     */
    ConstType const& constant() const {
        if (m_type != Type::Const) {
            throw std::bad_cast();
        }
        return m_storage.get<ConstType>();
    }

    /**
     * @brief constant
     * @param t
     */
    void constant(ConstType const& t) {
        m_type = Type::Const;
        m_storage.set<ConstType>(t);
    }

    /**
     * @brief variable
     * @return
     */
    VarType& variable() {
        if (m_type != Type::Var) {
            throw std::bad_cast();
        }
        return m_storage.get<VarType>();
    }

    /**
     * @brief variable
     * @return
     */
    VarType const& variable() const {
        if (m_type != Type::Var) {
            throw std::bad_cast();
        }
        return m_storage.get<VarType>();
    }

    /**
     * @brief variable
     * @param t
     */
    void variable(VarType const& t) {
        m_type = Type::Var;
        m_storage.set<VarType>(t);
    }

    DSDL() noexcept : m_type(Type::Root), m_storage() {
        RootType d;
        m_storage.set<RootType>(d);  // NOLINT
    }

    /**
     * @brief DSDL
     * @param dsdl
     */
    DSDL(std::string const& dsdl) : DSDL() { parse(dsdl); }

    /**
     * @brief DSDL
     * @param other
     */
    DSDL(DSDL const& other) = default;

    /**
     * @brief operator=
     * @param other
     * @return
     */
    DSDL& operator=(DSDL const& other) = default;

    /**
     * @brief parse
     * @param dsdl
     */
    void parse(const std::string& dsdl);

    /**
     * @brief stringify
     * @return
     */
    std::string stringify() const noexcept;

    /**
     * @brief The Traverser class
     */
    class Traverser {
     public:
        Traverser() = default;
        /**
         * @brief run
         * @param dsdl
         */
        void run(DSDL::RootType const& dsdl);

     public:
        /**
         * @brief emitRootBegin
         */
        virtual void emitRootBegin() = 0;

        /**
         * @brief emitRootEnd
         */
        virtual void emitRootEnd() = 0;

        /**
         * @brief emitEnumeratorBegin
         * @param name
         */
        virtual void emitEnumeratorBegin(std::string const& name) = 0;

        /**
         * @brief emitEnumeratorEnd
         * @param name
         */
        virtual void emitEnumeratorEnd(std::string const& name) = 0;

        /**
         * @brief emitStructureBegin
         * @param name
         */
        virtual void emitStructureBegin(std::string const& name) = 0;

        /**
         * @brief emitStructureEnd
         * @param name
         */
        virtual void emitStructureEnd(std::string const& name) = 0;

        /**
         * @brief emitOptionBegin
         * @param name
         */
        virtual void emitOptionBegin(std::string const& name) = 0;

        /**
         * @brief emitOptionEnd
         * @param name
         */
        virtual void emitOptionEnd(std::string const& name) = 0;

        /**
         * @brief emitComment
         * @param value
         */
        virtual void emitComment(std::string const& value) = 0;

        /**
         * @brief emitVariable
         * @param name
         * @param type
         */
        virtual void emitVariable(std::string const& name, std::string const& type) = 0;

        /**
         * @brief emitConstant
         * @param name
         * @param expr
         */
        virtual void emitConstant(std::string const& name, std::int64_t expr) = 0;

        /**
         * @brief emitEnumeratorEntry
         * @param name
         * @param expr
         */
        virtual void emitEnumeratorEntry(std::string const& name, std::int64_t expr) = 0;

        /**
         * @brief emitOptionAlternatives
         * @param alternatives
         */
        virtual void emitOptionAlternatives(std::list<std::pair<std::string, std::string>> const& alternatives) = 0;

        /**
         * @brief emitArray
         * @param name
         * @param type
         * @param sizeExpr
         */
        virtual void emitArray(std::string const& name, std::string const& type, std::uint64_t sizeExpr) = 0;

        virtual ~Traverser() = default;

     private:
        void run(DSDL const& dsdl);

     protected:
        /**
         * @brief parentNames used to retrieve currently processed object ancestor names
         * @return
         */
        std::list<std::string> const& parentNames() const;

     private:
        struct ExprToken {
            enum class Type { Operator, LeftParen, RightParen, Value };
            Type type;
            std::string data;
            std::uint8_t precedence;
        };
        std::list<ExprToken> getExprTokens(std::string const& expr) const;
        std::list<ExprToken> shuntingYard(std::list<ExprToken> const& tokens) const;
        std::int64_t evalReversePolishNotation(std::list<ExprToken> const& tokens) const;
        std::string getExprConstValue(std::string const& constName) const;
        std::int64_t evalExpression(std::string const& expr) const;
        void storeConstantValue(std::list<std::string> const& parents, std::string const& localName,
                                std::int64_t value);
        std::string createConstantUniqueName(std::list<std::string> const& parents, std::string const& localName) const;

     private:
        std::list<std::string> m_parentNames;
        std::map<std::string, std::int64_t> m_constants;
    };

 private:
    Type m_type;
    Variant<RootType, OptionType, ArrayType, ConstType, VarType, EnumType, StructType, CommentType> m_storage;
};
}  // namespace rili
